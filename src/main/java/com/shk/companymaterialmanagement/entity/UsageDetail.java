package com.shk.companymaterialmanagement.entity;

import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import com.shk.companymaterialmanagement.model.EmployeeRequest;
import com.shk.companymaterialmanagement.model.UsageDetailRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "materialId", nullable = false)
    private Material material;

    @Column(nullable = false)
    private LocalDateTime dateUse;

    public void putDateUse(UsageDetailRequest request) {
        this.dateUse = request.getDateUse();
    }

    private UsageDetail(UsageDetailBuilder builder) {
        this.employee = builder.employee;
        this.material = builder.material;
        this.dateUse = builder.dateUse;
    }

    public static class UsageDetailBuilder implements CommonModelBuilder<UsageDetail> {

        private final Employee employee;
        private final Material material;
        private final LocalDateTime dateUse;

        public UsageDetailBuilder(Employee employee, Material material, LocalDateTime dateUse) {

            this.employee = employee;
            this.material = material;
            this.dateUse = dateUse;
        }

        @Override
        public UsageDetail build() {
            return new UsageDetail(this);
        }
    }
}
