package com.shk.companymaterialmanagement.entity;

import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import com.shk.companymaterialmanagement.model.MaterialNameUpdateRequest;
import com.shk.companymaterialmanagement.model.MaterialRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String materialName;

    @Column(nullable = false, length = 20)
    private String materialType;

    @Column(nullable = false)
    private LocalDate dateReceiving; // 입고일

    @Column(nullable = false)
    private Integer materialAmount; // 자재량

    @Column(nullable = false)
    private Double materialPrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDataName(MaterialNameUpdateRequest updateRequest) {
        this.materialName = updateRequest.getMaterialName();
    }

    private Material(MaterialBuilder builder) {
        this.materialName = builder.materialName;
        this.materialType = builder.materialType;
        this.dateReceiving = builder.dateReceiving;
        this.materialAmount = builder.materialAmount;
        this.materialPrice = builder.materialPrice;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MaterialBuilder implements CommonModelBuilder<Material> {

        private final String materialName;
        private final String materialType;
        private final LocalDate dateReceiving;
        private final Integer materialAmount;
        private final Double materialPrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MaterialBuilder(MaterialRequest request) {
            this.materialName = request.getMaterialName();
            this.materialType = request.getMaterialType();
            this.dateReceiving = request.getDateReceiving();
            this.materialAmount = request.getMaterialAmount();
            this.materialPrice = request.getMaterialPrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Material build() {
            return new Material(this);
        }
    }
}
