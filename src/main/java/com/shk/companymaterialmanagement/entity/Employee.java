package com.shk.companymaterialmanagement.entity;

import com.shk.companymaterialmanagement.enums.EmployeeGender;
import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import com.shk.companymaterialmanagement.model.EmployeeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String employeeName;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 5)
    private EmployeeGender employeeGender;

    @Column(nullable = false)
    private LocalDate employeeDateJoin;

    @Column(nullable = false, length = 20)
    private String employeePhone;

    @Column(nullable = false, length = 50)
    private String employeeAddress;

    @Column(nullable = false, length = 10)
    private String employeeDepartment;

    public void putEmployee(EmployeeRequest request) {
        this.employeeName = request.getEmployeeName();
        this.employeeGender = request.getEmployeeGender();
        this.employeeDateJoin = request.getEmployeeDateJoin();
        this.employeePhone = request.getEmployeePhone();
        this.employeeAddress = request.getEmployeeAddress();
        this.employeeDepartment = request.getEmployeeDepartment();
    }

    private Employee(EmployeeBuilder builder) {
        this.employeeName = builder.employeeName;
        this.employeeGender = builder.employeeGender;
        this.employeeDateJoin = builder.employeeDateJoin;
        this.employeePhone = builder.employeePhone;
        this.employeeAddress = builder.employeeAddress;
        this.employeeDepartment = builder.employeeDepartment;
    }

    public static class EmployeeBuilder implements CommonModelBuilder<Employee> {

        private final String employeeName;
        private final EmployeeGender employeeGender;
        private final LocalDate employeeDateJoin;
        private final String employeePhone;
        private final String employeeAddress;
        private final String employeeDepartment;

        public EmployeeBuilder(EmployeeRequest request) {
            this.employeeName = request.getEmployeeName();
            this.employeeGender = request.getEmployeeGender();
            this.employeeDateJoin = request.getEmployeeDateJoin();
            this.employeePhone = request.getEmployeePhone();
            this.employeeAddress = request.getEmployeeAddress();
            this.employeeDepartment = request.getEmployeeDepartment();
        }

        @Override
        public Employee build() {

            return new Employee(this);
        }
    }
}
