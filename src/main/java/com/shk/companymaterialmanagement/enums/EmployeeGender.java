package com.shk.companymaterialmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmployeeGender {
    MAN("남자"),
    WOMAN("여자");

    private final String name;
}
