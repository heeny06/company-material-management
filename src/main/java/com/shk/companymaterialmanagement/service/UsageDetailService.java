package com.shk.companymaterialmanagement.service;

import com.shk.companymaterialmanagement.entity.Employee;
import com.shk.companymaterialmanagement.entity.Material;
import com.shk.companymaterialmanagement.entity.UsageDetail;
import com.shk.companymaterialmanagement.exception.CMissingDataException;
import com.shk.companymaterialmanagement.model.ListResult;
import com.shk.companymaterialmanagement.model.UsageDetailItem;
import com.shk.companymaterialmanagement.model.UsageDetailRequest;
import com.shk.companymaterialmanagement.repository.UsageDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailService {

    private final UsageDetailRepository usageDetailRepository;

    public void setUsageDetail(Employee employee, Material material, LocalDateTime dateUse) {
        UsageDetail usageDetail = new UsageDetail.UsageDetailBuilder(employee, material, dateUse).build();
        usageDetailRepository.save(usageDetail);
    }

    public UsageDetailItem getUsageDetail(long id) {
        UsageDetail usageDetail = usageDetailRepository.findById(id).orElseThrow(CMissingDataException::new);

        UsageDetailItem usageDetailItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();

        return usageDetailItem;
    }

    public ListResult<UsageDetailItem> getUsageDetails() {
        List<UsageDetail> usageDetails = usageDetailRepository.findAll();

        List<UsageDetailItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putUsageDetail(long id, UsageDetailRequest request) {
        UsageDetail usageDetail = usageDetailRepository.findById(id).orElseThrow(CMissingDataException::new);
        usageDetail.putDateUse(request);

        usageDetailRepository.save(usageDetail);
    }
}
