package com.shk.companymaterialmanagement.service;

import com.shk.companymaterialmanagement.entity.Material;
import com.shk.companymaterialmanagement.exception.CMissingDataException;
import com.shk.companymaterialmanagement.model.*;
import com.shk.companymaterialmanagement.repository.MaterialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MaterialService {

    private final MaterialRepository materialRepository;

    public Material getMaterialData(long id) {
        return materialRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setMaterial(MaterialRequest request) {
        Material material = new Material.MaterialBuilder(request).build();
        materialRepository.save(material);
    }

    public MaterialDetail getMaterial(long id) {
        Material material = materialRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new MaterialDetail.MaterialDetailBuilder(material).build();
    }

    public ListResult<MaterialItem> getMaterials() {
        List<Material> materials = materialRepository.findAll();

        List<MaterialItem> result = new LinkedList<>();

        materials.forEach(material -> {
            MaterialItem addItem = new MaterialItem.MaterialItemBuilder(material).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putMaterialName(long id, MaterialNameUpdateRequest updateRequest) {
        Material material = materialRepository.findById(id).orElseThrow(CMissingDataException::new);
        material.putDataName(updateRequest);
        materialRepository.save(material);
    }
}
