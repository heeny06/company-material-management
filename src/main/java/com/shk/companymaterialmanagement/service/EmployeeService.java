package com.shk.companymaterialmanagement.service;

import com.shk.companymaterialmanagement.entity.Employee;
import com.shk.companymaterialmanagement.exception.CMissingDataException;
import com.shk.companymaterialmanagement.model.EmployeeItem;
import com.shk.companymaterialmanagement.model.EmployeeRequest;
import com.shk.companymaterialmanagement.model.ListResult;
import com.shk.companymaterialmanagement.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public Employee getEmployeeData(long id) {
        return employeeRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setEmployee(EmployeeRequest request) {
        Employee employee = new Employee.EmployeeBuilder(request).build();
        employeeRepository.save(employee);
    }

    public EmployeeItem getEmployee(long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDataException::new);

        EmployeeItem employeeItem = new EmployeeItem.EmployeeItemBuilder(employee).build();

        return employeeItem;
    }

    public ListResult<EmployeeItem> getEmployees() {
        List<Employee> employees = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        employees.forEach(employee -> {
            EmployeeItem addItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putEmployee(long id, EmployeeRequest request) {
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDataException::new);
        employee.putEmployee(request);

        employeeRepository.save(employee);
    }
}
