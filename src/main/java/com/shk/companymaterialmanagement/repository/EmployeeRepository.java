package com.shk.companymaterialmanagement.repository;

import com.shk.companymaterialmanagement.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
