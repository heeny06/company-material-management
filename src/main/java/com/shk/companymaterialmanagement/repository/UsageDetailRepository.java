package com.shk.companymaterialmanagement.repository;

import com.shk.companymaterialmanagement.entity.UsageDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsageDetailRepository extends JpaRepository<UsageDetail, Long> {
}
