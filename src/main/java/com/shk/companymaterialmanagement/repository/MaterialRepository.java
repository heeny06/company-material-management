package com.shk.companymaterialmanagement.repository;

import com.shk.companymaterialmanagement.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialRepository extends JpaRepository<Material, Long> {
}
