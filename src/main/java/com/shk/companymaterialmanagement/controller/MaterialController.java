package com.shk.companymaterialmanagement.controller;

import com.shk.companymaterialmanagement.model.*;
import com.shk.companymaterialmanagement.service.MaterialService;
import com.shk.companymaterialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "자재 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/material")
public class MaterialController {

    public final MaterialService materialService;

    @ApiOperation(value = "자재 정보 등록")
    @PostMapping("/new")
    public CommonResult setMaterial(@RequestBody @Valid MaterialRequest request) {
        materialService.setMaterial(request);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "자재 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "자재시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<MaterialDetail> getMaterial(@PathVariable long id) {
        return ResponseService.getSingleResult(materialService.getMaterial(id));
    }

    @ApiOperation(value = "자재 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<MaterialItem> getMaterials() {
        return ResponseService.getListResult(materialService.getMaterials(), true);
    }

    @ApiOperation(value = "자재 정보 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "자재시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putMaterialName(@PathVariable long id, @RequestBody @Valid MaterialNameUpdateRequest updateRequest) {
        materialService.putMaterialName(id, updateRequest);
        return ResponseService.getSuccessResult();
    }
}
