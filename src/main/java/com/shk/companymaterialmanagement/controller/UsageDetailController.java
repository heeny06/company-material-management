package com.shk.companymaterialmanagement.controller;

import com.shk.companymaterialmanagement.entity.Employee;
import com.shk.companymaterialmanagement.entity.Material;
import com.shk.companymaterialmanagement.entity.UsageDetail;
import com.shk.companymaterialmanagement.model.*;
import com.shk.companymaterialmanagement.service.EmployeeService;
import com.shk.companymaterialmanagement.service.MaterialService;
import com.shk.companymaterialmanagement.service.ResponseService;
import com.shk.companymaterialmanagement.service.UsageDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-detail")
public class UsageDetailController {

    private final EmployeeService employeeService;

    private final MaterialService materialService;

    private final UsageDetailService usageDetailService;

    @ApiOperation(value = "사용내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetail(@RequestBody @Valid UsageDetailRequest request) {
        Employee employee = employeeService.getEmployeeData(request.getEmployeeId());
        Material material = materialService.getMaterialData(request.getMaterialId());

        usageDetailService.setUsageDetail(employee, material, request.getDateUse());

        return ResponseService.getSuccessResult();

    }

    @ApiOperation(value = "사용내역 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사용내역시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<UsageDetailItem> getUsageDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(usageDetailService.getUsageDetail(id));
    }

    @ApiOperation(value = "사용내역 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<UsageDetailItem> getUsageDetails() {
        return ResponseService.getListResult(usageDetailService.getUsageDetails(), true);
    }

    @ApiOperation(value = "사용내역 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사용내역시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putUsageDetail(@PathVariable long id, @RequestBody @Valid UsageDetailRequest request) {
        usageDetailService.putUsageDetail(id, request);
        return ResponseService.getSuccessResult();
    }
}
