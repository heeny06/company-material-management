package com.shk.companymaterialmanagement.controller;

import com.shk.companymaterialmanagement.model.*;
import com.shk.companymaterialmanagement.service.EmployeeService;
import com.shk.companymaterialmanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @ApiOperation(value = "사원 정보 등록")
    @PostMapping("/new")
    public CommonResult setEmployee(@RequestBody @Valid EmployeeRequest request) {
        employeeService.setEmployee(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<EmployeeItem> getEmployee(@PathVariable long id) {
        return ResponseService.getSingleResult(employeeService.getEmployee(id));
    }

    @ApiOperation(value = "사원 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<EmployeeItem> getEmployees() {
        return ResponseService.getListResult(employeeService.getEmployees(), true);
    }

    @ApiOperation(value = "사원 정보 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "사원시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putEmployee(@PathVariable long id, @RequestBody @Valid EmployeeRequest request) {
        employeeService.putEmployee(id, request);
        return ResponseService.getSuccessResult();
    }
}
