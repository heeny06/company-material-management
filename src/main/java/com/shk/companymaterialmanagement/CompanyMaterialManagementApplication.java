package com.shk.companymaterialmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyMaterialManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyMaterialManagementApplication.class, args);
	}

}
