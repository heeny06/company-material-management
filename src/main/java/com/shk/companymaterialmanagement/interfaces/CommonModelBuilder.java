package com.shk.companymaterialmanagement.interfaces;

public interface CommonModelBuilder<T> {
    T build(); // 할일을 정해주는것
}
