package com.shk.companymaterialmanagement.exception;

public class CMissingDataException extends RuntimeException { // 출구의 기능을 상속 받아야함
    public CMissingDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingDataException(String msg) {
        super(msg);
    }

    public CMissingDataException() {
        super();
    }
}
