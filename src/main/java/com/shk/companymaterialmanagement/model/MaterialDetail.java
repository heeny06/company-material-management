package com.shk.companymaterialmanagement.model;

import com.shk.companymaterialmanagement.entity.Material;
import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialDetail {

    @ApiModelProperty(notes = "자재시퀀스")
    private Long id;

    @ApiModelProperty(notes = "자재이름")
    private String materialName;

    @ApiModelProperty(notes = "자재타입")
    private String materialType;

    @ApiModelProperty(notes = "입고일")
    private LocalDate dateReceiving;

    @ApiModelProperty(notes = "자재량")
    private Integer materialAmount;

    @ApiModelProperty(notes = "자재가격")
    private Double materialPrice;

    private MaterialDetail(MaterialDetailBuilder builder) {
        this.id = builder.id;
        this.materialName = builder.materialName;
        this.materialType = builder.materialType;
        this.dateReceiving = builder.dateReceiving;
        this.materialAmount = builder.materialAmount;
        this.materialPrice = builder.materialPrice;
    }

    public static class MaterialDetailBuilder implements CommonModelBuilder<MaterialDetail> {

        private final Long id;
        private final String materialName;
        private final String materialType;
        private final LocalDate dateReceiving;
        private final Integer materialAmount;
        private final Double materialPrice;

        public MaterialDetailBuilder(Material material) {
            this.id = material.getId();
            this.materialName = material.getMaterialName();
            this.materialType = material.getMaterialType();
            this.dateReceiving = material.getDateReceiving();
            this.materialAmount = material.getMaterialAmount();
            this.materialPrice = material.getMaterialPrice();
        }

        @Override
        public MaterialDetail build() {
            return new MaterialDetail(this);
        }
    }
}
