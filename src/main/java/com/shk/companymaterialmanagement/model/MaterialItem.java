package com.shk.companymaterialmanagement.model;

import com.shk.companymaterialmanagement.entity.Material;
import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialItem {

    @ApiModelProperty(notes = "자재시퀀스")
    private Long id;

    @ApiModelProperty(notes = "자재풀네임")
    private String MaterialFullName;

    @ApiModelProperty(notes = "자재량")
    private String materialAmount;

    @ApiModelProperty(notes = "자재가격")
    private String materialPrice;

    private MaterialItem(MaterialItemBuilder builder) {
        this.id = builder.id;
        this.MaterialFullName = builder.MaterialFullName;
        this.materialAmount = builder.materialAmount;
        this.materialPrice = builder.materialPrice;
    }

    public static class MaterialItemBuilder implements CommonModelBuilder<MaterialItem> {

        private final Long id;
        private final String MaterialFullName;
        private final String materialAmount;
        private final String materialPrice;

        public MaterialItemBuilder(Material material) {
            this.id = material.getId();
            this.MaterialFullName = "[" + material.getMaterialType() + "] " + material.getMaterialName();
            this.materialAmount = "[" + material.getMaterialAmount() + "Kg]";
            this.materialPrice = "[" + material.getMaterialPrice() + "만원]";
        }

        @Override
        public MaterialItem build() {

            return new MaterialItem(this);
        }
    }

}
