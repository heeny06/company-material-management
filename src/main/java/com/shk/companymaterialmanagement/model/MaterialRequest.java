package com.shk.companymaterialmanagement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MaterialRequest {

    @ApiModelProperty(notes = "자재이름", required = true)
    @NotNull
    @Length(max = 20)
    private String materialName;

    @ApiModelProperty(notes = "자재타입", required = true)
    @NotNull
    @Length(max = 20)
    private String materialType;

    @ApiModelProperty(notes = "입고일", required = true)
    @NotNull
    private LocalDate dateReceiving;

    @ApiModelProperty(notes = "자재량", required = true)
    @NotNull
    private Integer materialAmount;

    @ApiModelProperty(notes = "자재가격", required = true)
    @NotNull
    private Double materialPrice;
}
