package com.shk.companymaterialmanagement.model;

import com.shk.companymaterialmanagement.enums.EmployeeGender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EmployeeRequest {

    @ApiModelProperty(notes = "사원이름", required = true)
    @NotNull
    @Length(max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "사원성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EmployeeGender employeeGender;

    @ApiModelProperty(notes = "사원입사일", required = true)
    @NotNull
    private LocalDate employeeDateJoin;

    @ApiModelProperty(notes = "사원연락처", required = true)
    @NotNull
    @Length(max = 20)
    private String employeePhone;

    @ApiModelProperty(notes = "사원주소", required = true)
    @NotNull
    @Length(max = 50)
    private String employeeAddress;

    @ApiModelProperty(notes = "사원부서", required = true)
    @NotNull
    @Length(max = 10)
    private String employeeDepartment;

}
