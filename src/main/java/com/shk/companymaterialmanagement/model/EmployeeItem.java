package com.shk.companymaterialmanagement.model;

import com.shk.companymaterialmanagement.entity.Employee;
import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeItem {

    @ApiModelProperty(notes = "사원시퀀스")
    private Long id;

    @ApiModelProperty(notes = "사원풀네임")
    private String employeeFullName;

    @ApiModelProperty(notes = "사원입사일")
    private LocalDate employeeDateJoin;

    private EmployeeItem(EmployeeItemBuilder builder) {
        this.id = builder.id;
        this.employeeFullName = builder.employeeFullName;
        this.employeeDateJoin = builder.employeeDateJoin;
    }

    public static class EmployeeItemBuilder implements CommonModelBuilder<EmployeeItem> {

        private final Long id;
        private final String employeeFullName;
        private final LocalDate employeeDateJoin;

        public EmployeeItemBuilder(Employee employee) {
            this.id = employee.getId();
            this.employeeFullName = "[" + employee.getEmployeeDepartment() + "] " + employee.getEmployeeName();
            this.employeeDateJoin = employee.getEmployeeDateJoin();
        }

        @Override
        public EmployeeItem build() {
            return new EmployeeItem(this);
        }
    }
}
