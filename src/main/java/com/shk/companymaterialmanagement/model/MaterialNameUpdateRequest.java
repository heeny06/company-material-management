package com.shk.companymaterialmanagement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MaterialNameUpdateRequest {

    @ApiModelProperty(notes = "자재이름")
    private String materialName;
}
