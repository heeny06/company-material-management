package com.shk.companymaterialmanagement.model;


import com.shk.companymaterialmanagement.entity.UsageDetail;
import com.shk.companymaterialmanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailItem {

    @ApiModelProperty(notes = "사용내역 시퀀스")
    private Long UsageDetailId;

    @ApiModelProperty(notes = "사용일자")
    private LocalDateTime dateUse;

    @ApiModelProperty(notes = "사원 시퀀스")
    private Long EmployeeId;

    @ApiModelProperty(notes = "사원전체이름")
    private String employeeFullName;

    @ApiModelProperty(notes = "자재 시퀀스")
    private Long MaterialId;

    @ApiModelProperty(notes = "자재전체이름")
    private String MaterialFullName;

    private UsageDetailItem(UsageDetailItemBuilder builder) {
        this.UsageDetailId = builder.UsageDetailId;
        this.dateUse = builder.dateUse;
        this.EmployeeId = builder.EmployeeId;
        this.employeeFullName = builder.employeeFullName;
        this.MaterialId = builder.MaterialId;
        this.MaterialFullName = builder.MaterialFullName;
    }

    public static class UsageDetailItemBuilder implements CommonModelBuilder<UsageDetailItem> {

        private final Long UsageDetailId;
        private final LocalDateTime dateUse;
        private final Long EmployeeId;
        private final String employeeFullName;
        private final Long MaterialId;
        private final String MaterialFullName;

        public UsageDetailItemBuilder(UsageDetail usageDetail) {
            this.UsageDetailId = usageDetail.getId();
            this.dateUse = usageDetail.getDateUse();
            this.EmployeeId = usageDetail.getEmployee().getId();
            this.employeeFullName = "[" + usageDetail.getEmployee().getEmployeeDepartment() + "] " + usageDetail.getEmployee().getEmployeeName();
            this.MaterialId = usageDetail.getMaterial().getId();
            this.MaterialFullName = "[" + usageDetail.getMaterial().getMaterialType() + "] " + usageDetail.getMaterial().getMaterialName();
        }

        @Override
        public UsageDetailItem build() {
            return new UsageDetailItem(this);
        }
    }
}
