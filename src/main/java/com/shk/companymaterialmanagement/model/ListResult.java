package com.shk.companymaterialmanagement.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult { // 무언가의 타입을 하나줘
    private List<T> list;

    private Long totalItemCount; // 총 아이템이 몇개인지

    private Integer totalPage; // 총페이지수

    private Integer currentPage; // 내가 지금 몇번째 페이지인지
}
