package com.shk.companymaterialmanagement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailRequest {

    @ApiModelProperty(notes = "사원 아이디", required = true)
    @NotNull
    private Long employeeId;

    @ApiModelProperty(notes = "자재 아이디", required = true)
    @NotNull
    private Long materialId;

    @ApiModelProperty(notes = "사용날짜", required = true)
    @NotNull
    private LocalDateTime dateUse;
}
