package com.shk.companymaterialmanagement.advice;

import com.shk.companymaterialmanagement.enums.ResultCode;
import com.shk.companymaterialmanagement.exception.CMissingDataException;
import com.shk.companymaterialmanagement.exception.CNoMemberDataException;
import com.shk.companymaterialmanagement.exception.CWrongPhoneNumberException;
import com.shk.companymaterialmanagement.model.CommonResult;
import com.shk.companymaterialmanagement.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { // 기본 비상구, 기본 실패하였습니다.
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 사용자의 잘못이면 400번 에러 보여줌
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 잘못된 번호면 에러 보여줌
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CNoMemberDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 탈퇴한 회원일 경우 에러를 보여줌
    protected CommonResult customException(HttpServletRequest request, CNoMemberDataException e) {
        return ResponseService.getFailResult(ResultCode.N0_MEMBER_DATA);
    }
}
